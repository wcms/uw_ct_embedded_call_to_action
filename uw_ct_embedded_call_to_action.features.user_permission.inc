<?php

/**
 * @file
 * uw_ct_embedded_call_to_action.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_embedded_call_to_action_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_embedded_call_to_action content'.
  $permissions['create uw_embedded_call_to_action content'] = array(
    'name' => 'create uw_embedded_call_to_action content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_embedded_call_to_action content'.
  $permissions['delete any uw_embedded_call_to_action content'] = array(
    'name' => 'delete any uw_embedded_call_to_action content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_embedded_call_to_action content'.
  $permissions['delete own uw_embedded_call_to_action content'] = array(
    'name' => 'delete own uw_embedded_call_to_action content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_embedded_call_to_action content'.
  $permissions['edit any uw_embedded_call_to_action content'] = array(
    'name' => 'edit any uw_embedded_call_to_action content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_embedded_call_to_action content'.
  $permissions['edit own uw_embedded_call_to_action content'] = array(
    'name' => 'edit own uw_embedded_call_to_action content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_embedded_call_to_action revision log entry'.
  $permissions['enter uw_embedded_call_to_action revision log entry'] = array(
    'name' => 'enter uw_embedded_call_to_action revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_call_to_action authored by option'.
  $permissions['override uw_embedded_call_to_action authored by option'] = array(
    'name' => 'override uw_embedded_call_to_action authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_call_to_action authored on option'.
  $permissions['override uw_embedded_call_to_action authored on option'] = array(
    'name' => 'override uw_embedded_call_to_action authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_call_to_action promote to front page option'.
  $permissions['override uw_embedded_call_to_action promote to front page option'] = array(
    'name' => 'override uw_embedded_call_to_action promote to front page option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_call_to_action published option'.
  $permissions['override uw_embedded_call_to_action published option'] = array(
    'name' => 'override uw_embedded_call_to_action published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_call_to_action revision option'.
  $permissions['override uw_embedded_call_to_action revision option'] = array(
    'name' => 'override uw_embedded_call_to_action revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_embedded_call_to_action sticky option'.
  $permissions['override uw_embedded_call_to_action sticky option'] = array(
    'name' => 'override uw_embedded_call_to_action sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_embedded_call_to_action content'.
  $permissions['search uw_embedded_call_to_action content'] = array(
    'name' => 'search uw_embedded_call_to_action content',
    'roles' => array(),
    'module' => 'search_config',
  );

  return $permissions;
}
