<?php

/**
 * @file
 * uw_ct_embedded_call_to_action.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_ct_embedded_call_to_action_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_uw_embedded_call_to_action';
  $strongarm->value = 'edit-uw-page-settings-node';
  $export['additional_settings__active_tab_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_uw_embedded_call_to_action';
  $strongarm->value = '0';
  $export['comment_anonymous_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_uw_embedded_call_to_action';
  $strongarm->value = 1;
  $export['comment_default_mode_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_uw_embedded_call_to_action';
  $strongarm->value = '50';
  $export['comment_default_per_page_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_uw_embedded_call_to_action';
  $strongarm->value = 1;
  $export['comment_form_location_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_uw_embedded_call_to_action';
  $strongarm->value = '1';
  $export['comment_preview_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_uw_embedded_call_to_action';
  $strongarm->value = 1;
  $export['comment_subject_field_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_uw_embedded_call_to_action';
  $strongarm->value = '1';
  $export['comment_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'default_menu_link_enabled_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['default_menu_link_enabled_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_uw_embedded_call_to_action';
  $strongarm->value = 1;
  $export['diff_enable_revisions_page_node_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_uw_embedded_call_to_action';
  $strongarm->value = 1;
  $export['diff_show_preview_changes_node_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_uw_embedded_call_to_action';
  $strongarm->value = 'full';
  $export['diff_view_mode_preview_node_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_translation_comment_filter_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['entity_translation_comment_filter_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_translation_hide_translation_links_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['entity_translation_hide_translation_links_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_translation_node_metadata_uw_embedded_call_to_action';
  $strongarm->value = '0';
  $export['entity_translation_node_metadata_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__uw_embedded_call_to_action';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'locations' => array(
          'weight' => '30',
        ),
        'metatags' => array(
          'weight' => '40',
        ),
        'title' => array(
          'weight' => '-5',
        ),
        'path' => array(
          'weight' => '30',
        ),
        'redirect' => array(
          'weight' => '30',
        ),
        'xmlsitemap' => array(
          'weight' => '30',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'forward_display_uw_embedded_call_to_action';
  $strongarm->value = 1;
  $export['forward_display_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_uw_embedded_call_to_action';
  $strongarm->value = '0';
  $export['language_content_type_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'linkchecker_scan_comment_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['linkchecker_scan_comment_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'linkchecker_scan_node_uw_embedded_call_to_action';
  $strongarm->value = 1;
  $export['linkchecker_scan_node_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mb_content_cancel_uw_embedded_call_to_action';
  $strongarm->value = '0';
  $export['mb_content_cancel_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mb_content_sac_uw_embedded_call_to_action';
  $strongarm->value = '0';
  $export['mb_content_sac_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mb_content_tabcn_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['mb_content_tabcn_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_uw_embedded_call_to_action';
  $strongarm->value = array();
  $export['menu_options_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_uw_embedded_call_to_action';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_node__uw_embedded_call_to_action';
  $strongarm->value = TRUE;
  $export['metatag_enable_node__uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_uw_embedded_call_to_action';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_uw_embedded_call_to_action';
  $strongarm->value = '0';
  $export['node_preview_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_revision_delete_number_uw_embedded_call_to_action';
  $strongarm->value = '50';
  $export['node_revision_delete_number_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_revision_delete_track_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['node_revision_delete_track_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_uw_embedded_call_to_action';
  $strongarm->value = 1;
  $export['node_submitted_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_title_type_uw_embedded_call_to_action';
  $strongarm->value = '';
  $export['page_title_type_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_title_type_uw_embedded_call_to_action_showfield';
  $strongarm->value = 0;
  $export['page_title_type_uw_embedded_call_to_action_showfield'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_expand_fieldset_uw_embedded_call_to_action';
  $strongarm->value = '0';
  $export['scheduler_expand_fieldset_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_enable_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['scheduler_publish_enable_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_moderation_state_uw_embedded_call_to_action';
  $strongarm->value = 'published';
  $export['scheduler_publish_moderation_state_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_past_date_uw_embedded_call_to_action';
  $strongarm->value = 'error';
  $export['scheduler_publish_past_date_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_required_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['scheduler_publish_required_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_revision_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['scheduler_publish_revision_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_touch_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['scheduler_publish_touch_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_default_time_uw_embedded_call_to_action';
  $strongarm->value = '';
  $export['scheduler_unpublish_default_time_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_enable_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['scheduler_unpublish_enable_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_moderation_state_uw_embedded_call_to_action';
  $strongarm->value = 'draft';
  $export['scheduler_unpublish_moderation_state_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_required_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['scheduler_unpublish_required_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_revision_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['scheduler_unpublish_revision_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_use_vertical_tabs_uw_embedded_call_to_action';
  $strongarm->value = '1';
  $export['scheduler_use_vertical_tabs_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_page_settings_node_node_type_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['uw_page_settings_node_node_type_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'webform_node_uw_embedded_call_to_action';
  $strongarm->value = 0;
  $export['webform_node_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_moderation_default_state_uw_embedded_call_to_action';
  $strongarm->value = 'draft';
  $export['workbench_moderation_default_state_uw_embedded_call_to_action'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'xmlsitemap_settings_node_uw_embedded_call_to_action';
  $strongarm->value = array(
    'status' => '0',
    'priority' => '0.5',
  );
  $export['xmlsitemap_settings_node_uw_embedded_call_to_action'] = $strongarm;

  return $export;
}
