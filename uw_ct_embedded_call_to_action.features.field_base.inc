<?php

/**
 * @file
 * uw_ct_embedded_call_to_action.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function uw_ct_embedded_call_to_action_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_call_to_action'.
  $field_bases['field_call_to_action'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_call_to_action',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'paragraphs',
    'settings' => array(
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'paragraphs',
  );

  // Exported field_base: 'field_cta_big_text'.
  $field_bases['field_cta_big_text'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_cta_big_text',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'max_length' => 52,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_cta_link'.
  $field_bases['field_cta_link'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_cta_link',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(),
    'locked' => 0,
    'module' => 'link',
    'settings' => array(
      'attributes' => array(
        'class' => '',
        'rel' => '',
        'target' => 'default',
      ),
      'display' => array(
        'url_cutoff' => 80,
      ),
      'enable_tokens' => 1,
      'entity_translation_sync' => FALSE,
      'title' => 'optional',
      'title_maxlength' => 128,
      'title_value' => '',
      'url' => 0,
    ),
    'translatable' => 0,
    'type' => 'link_field',
  );

  // Exported field_base: 'field_cta_small_text'.
  $field_bases['field_cta_small_text'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_cta_small_text',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'max_length' => 42,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_cta_theme'.
  $field_bases['field_cta_theme'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_cta_theme',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'uWaterloo' => 'uWaterloo (black and gold)',
        'neutral' => 'Neutral (Grey)',
        'arts' => 'Arts (orange)',
        'engineering' => 'Engineering (purple)',
        'environment' => 'Environment (green)',
        'ahs' => 'Health (teal)',
        'math' => 'Math (pink)',
        'science' => 'Science (blue)',
        'conrad' => 'Conrad Grebel (red)',
        'renison' => 'Renison (green)',
        'schools' => 'Schools (Red)',
        'st-jeromes' => 'St. Jerome\'s (gold)',
        'st-pauls' => 'United (green)',
      ),
      'allowed_values_function' => '',
      'entity_translation_sync' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
