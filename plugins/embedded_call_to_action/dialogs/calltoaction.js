/**
 * @file
 */

(function () {
  // Variable used to track when the form is submitted.
  // i.e to check for double submissions on using enter key in dialog.
  var formSubmitted = false;
  var calltoactionDialog = function (editor) {
    return {
      title : 'Call to Action Properties',
      minWidth : 625,
      minHeight : 150,
      contents: [{
        id: 'calltoaction',
        label: 'calltoaction',
        elements:[{
          type: 'text',
          id: 'calltoaction-nid',
          label: 'Please enter embedded call to action node ID:',
          required: true,
          setup: function (element) {
            this.setValue(element.getAttribute('data-calltoaction-nid'));
          }
        }]
      }],
      onOk: function () {
        // Get form information.
        calltoaction_nid = this.getValueOf('calltoaction','calltoaction-nid');

        // Validate input. Note that there is probably a CKEditor specific way to do this, but this works.
        errors = "";
        if (!CKEDITOR.embedded_calltoaction.calltoaction_regex.test(calltoaction_nid)) {
            errors += "You must enter a node ID.\r\n";
        }
        else {
          errors = '';
        }
        if (!calltoaction_nid) {
          errors = "You must enter a node ID.\r\n";
        }

        if (errors == '') {
          // Variable used to store number of call to action (call to php function).
          var num_of_cta;

          // Use jQuery ajax call to check if nid supplied is a call to action.
          jQuery(function ($) {

            // Make the ajax call, set to synchronous so that we wait to get a response.
            $.ajax({
              type: 'get',
              async: false,
              url: Drupal.settings.basePath + "ajax/nid_exists/" + calltoaction_nid + "/uw_embedded_call_to_action",
              // If we have success set to the global variable, so that we can use it later to check for errors.
              success: function (data) {
                num_of_cta = data;
              }
            });
          });

          // If the number of call to action is 0 (meaning there are no FF with that nid), set the errors.
          if (num_of_cta == 0) {
            errors = 'You must enter a valid call to action node ID.\r\n';
          }
        }

        // If form has been submitted before then set it back to not being seeing before.
        // i.e if this is double submission set it back to not being run before.
        if (formSubmitted == true) {
          formSubmitted = false;
          return false;
        }
        // Only display erros if there are errors to display and the form has not been run before.
         else if (errors && formSubmitted == false) {
          alert(errors);
          formSubmitted = true;
          return false;
        }
        else {
          // Create the facts/figures element.
          var ckcalltoactionNode = new CKEDITOR.dom.element('ckcalltoaction');
          // Save contents of dialog as attributes of the element.
          ckcalltoactionNode.setAttribute('data-calltoaction-nid', calltoaction_nid);

          // Adjust title based on user input.
          CKEDITOR.lang.en.fakeobjects.ckcalltoaction = CKEDITOR.embedded_calltoaction.ckcalltoaction + ': ' + calltoaction_nid;
          // Create the fake image for this element and insert into the document (realElement, className, realElementType, isResizable)
          var newFakeImage = editor.createFakeElement(ckcalltoactionNode, 'ckcalltoaction', 'ckcalltoaction', false);
          // Set the fake object to the entered height; if there isn't one, use 100 so it's not invisible.
          newFakeImage.addClass('ckcalltoaction');
          if (this.fakeImage) {
            newFakeImage.replace(this.fakeImage);
            editor.getSelection().selectElement(newFakeImage);
          }
          else {
            editor.insertElement(newFakeImage);
          }
          // Reset title.
          CKEDITOR.lang.en.fakeobjects.ckcalltoaction = CKEDITOR.embedded_calltoaction.ckcalltoaction;
        }
      },
      onShow: function () {
        // Set up to handle existing items.
        this.fakeImage = this.ckcalltoactionNode = null;
        var fakeImage;

        // Check if element is right clicked or icon was clicked, if not use global varaible doubleclick_element.
        if (this.getSelectedElement()) {
          fakeImage = this.getSelectedElement();
        }
        else {
          fakeImage = fakeImage;
        }

        if (fakeImage && fakeImage.data('cke-real-element-type') && fakeImage.data('cke-real-element-type') === 'ckcalltoaction') {
          this.fakeImage = fakeImage;
          var ckcalltoactionNode = editor.restoreRealElement(fakeImage);
          this.ckcalltoactionNode = ckcalltoactionNode;
          this.setupContent(ckcalltoactionNode);
        }
      }
    }
  }

  CKEDITOR.dialog.add('calltoaction', function (editor) {
    return calltoactionDialog(editor);
  });
})();
