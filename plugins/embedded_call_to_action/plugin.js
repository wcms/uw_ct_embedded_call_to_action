/**
 * @file
 * CKEditor configuration.
 */

cktags = ['ckcalltoaction'];

// Global variable used to store fakeImage element when socialmedia is double clicked.
var doubleclick_element;

CKEDITOR.plugins.add('embedded_calltoaction', {
  requires : ['dialog', 'fakeobjects'],
  init: function (editor) {
    // Define plugin name.
    var pluginName = 'embedded_calltoaction';

    // Set up object to share variables amongst scripts.
    CKEDITOR.embedded_calltoaction = {};

    // Register button for Call to Action.
    editor.ui.addButton('Call to Action', {
      label : "Add/Edit Call to Action",
      command : 'calltoaction',
      icon: this.path + 'icons/call_to_action.png',
    });
    // Register right-click menu item for Call to Action.
    editor.addMenuItems({
      calltoaction : {
        label : "Edit Call to Action",
        icon: this.path + 'icons/call_to_action.png',
        command : 'calltoaction',
        group : 'image',
        order : 1
      }
    });

    // Configure DTD.
    CKEDITOR.dtd.ckcalltoaction = {};
    // Define element as "block level" so the editor doesn't wrap it in 'p'.
    CKEDITOR.dtd.$block.ckcalltoaction = 1;
    CKEDITOR.dtd.body.ckcalltoaction = 1;
    // Make ckcalltoaction to be a self-closing tag.
    CKEDITOR.dtd.$empty.ckcalltoaction = 1;

    // Make sure the fake element for Call to Action has a name.
    CKEDITOR.embedded_calltoaction.ckcalltoaction = 'calltoaction';
    CKEDITOR.lang.en.fakeobjects.ckcalltoaction = CKEDITOR.embedded_calltoaction.ckcalltoaction;
    // Add JavaScript file that defines the dialog box for Call to Action.
    CKEDITOR.dialog.add('calltoaction', this.path + 'dialogs/calltoaction.js');
    // Register command to open dialog box when button is clicked.
    editor.addCommand('calltoaction', new CKEDITOR.dialogCommand('calltoaction'));

    // Regular expressions for Call to Action.
    CKEDITOR.embedded_calltoaction.calltoaction_regex = /^[1-9][0-9]*$/;
    // Open the appropriate dialog box if an element is double-clicked.
    editor.on('doubleclick', function (evt) {
      var element = evt.data.element;

      // Store the element as global variable to be used in onShow of dialog, when double clicked.
      doubleclick_element = element;

      if (element.is('img') && element.data('cke-real-element-type') === 'ckcalltoaction') {
        evt.data.dialog = 'calltoaction';
      }
    });

    // Add the appropriate right-click menu item if an element is right-clicked.
    if (editor.contextMenu) {
      editor.contextMenu.addListener(function (element, selection) {
        if (element && element.is('img') && element.data('cke-real-element-type') === 'ckcalltoaction') {
          return { calltoaction : CKEDITOR.TRISTATE_OFF };
        }
      });
    }

    // Add CSS to use in-editor to style custom (fake) elements.
    CKEDITOR.addCss(

      'img.ckcalltoaction {' +
      'background-image: url(' + CKEDITOR.getUrl(this.path + 'icons/big/call_to_action.png') + ');' +
      'background-position: center center;' +
      'background-repeat: no-repeat;' +
      'background-color: #000;' +
      'width: 100%;' +
      'height: 100px;' +
      'margin: 0 0 10px 0;' +
      'margin-left: auto;' +
      'margin-right: auto;' +
    '}' +

      // Call to Action: Definitions for wide width.
        '.uw_tf_standard_wide p img.ckcalltoactin {' +
          'height: 200px;' +
        '}' +

        // Call to Action: Definitions for standard width.
        '.uw_tf_standard p img.calltoaction {' +
          'height: 200px;' +
        '}'
    );
  },
  afterInit : function (editor) {
    // Make fake image display on first load/return from source view.
    if (editor.dataProcessor.dataFilter) {
      editor.dataProcessor.dataFilter.addRules({
        elements : {
          ckcalltoaction : function (element) {
            // Reset title.
            CKEDITOR.lang.en.fakeobjects.ckcalltoaction = CKEDITOR.embedded_calltoaction.ckcalltoaction;
            // Adjust title if a list name is present.
            if (element.attributes['calltoaction-nid']) {
              CKEDITOR.lang.en.fakeobjects.ckcalltoaction += ': ' + element.attributes['calltoaction-nid'];
            }
            // Note that this just accepts whatever attributes are on the element; may want to filter these.
            return editor.createFakeParserElement(element, 'ckcalltoaction', 'ckcalltoaction', false);
          }
        }
      });
    }
  }
});
